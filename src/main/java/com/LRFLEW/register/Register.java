package com.LRFLEW.register;

import java.util.logging.Level;

import org.bukkit.plugin.PluginDescriptionFile;
import org.bukkit.plugin.java.JavaPlugin;

import com.LRFLEW.register.payment.Methods;

public class Register extends JavaPlugin {

	@Override
	public void onDisable() {
		Methods.reset();
		
		PluginDescriptionFile pdfFile = this.getDescription();
		System.out.println( pdfFile.getName() + " is disabled and methods have been reset!" );
	}

	@Override
	public void onEnable() {
		Methods.setVersion(this.getDescription().getVersion());
		
		Methods.setMethod(this.getServer().getPluginManager());
		
		PluginDescriptionFile pdfFile = this.getDescription();
		if (Methods.getMethod() == null) getServer().getLogger().log(Level.WARNING, 
				"[Register] No Method Found.  Plugins may not work");
		System.out.println( pdfFile.getName() + " version " + pdfFile.getVersion() + 
				" is enabled and available for hooking!" );
	}

}
